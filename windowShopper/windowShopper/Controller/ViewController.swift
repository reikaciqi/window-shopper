//
//  ViewController.swift
//  windowShopper
//
//  Created by Rei on 10/10/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
   
    @IBOutlet weak var wageTxt: CurrencyTextField!
    @IBOutlet weak var priceTxt: CurrencyTextField!
    
    //outlete per
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var hoursLbl: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // krijim i butonit programatikisht
        //percaktojme koordinatat gjeresine sa ajo e view dhe lartesine
        let calcBtn = UIButton(frame: CGRect(x: 0, y:0, width: view.frame.size.width, height: 60))
        
        calcBtn.backgroundColor = #colorLiteral(red: 0.88022995, green: 0.5617145896, blue: 0, alpha: 1)
        //titulli dhe karakteristikat e tij
        calcBtn.setTitle("Calculate", for:.normal)
        calcBtn.setTitleColor(UIColor.white, for: .normal)
        //specifikohet target si self, perzgjidhet me selctor metoda qe do thirret dhe ne fund eventi
        calcBtn.addTarget(self, action: #selector(ViewController.calculate), for: .touchUpInside)
        
        //kur kur klikohet ndonej nga text fields shfaqet butoni qe krijuam
        wageTxt.inputAccessoryView = calcBtn
        priceTxt.inputAccessoryView = calcBtn
        
        resultLbl.isHidden = true
        hoursLbl.isHidden = true
    }

    //implementohet funksioni qe kthen rezultatin
    @objc func calculate(){
        //nese fushat nuk jane nil
        if let wageTxt = wageTxt.text, let priceTxt = priceTxt.text {
            //nese vlerat e marra nga fushat jane tendryshme nga teksti dhe nese konvertohen ne double
            if let wage = Double(wageTxt), let price = Double(priceTxt){
                
                view.endEditing(true)
                resultLbl.isHidden = false
                hoursLbl.isHidden = false
                //thirret funksioni i ndertuar tek klasa wage dhe rezultati shkon tek label perkates
                resultLbl.text = "\(Wage.getHours(forWage: wage, andPrice: price))"
                
            }
            
        }
    }

    @IBAction func ClearCalculatorPressed(_ sender: Any) {
        //ne klikim te butonit 'clear calculator' pastrojme fushat dhe fshefim labelat e rezultatit
        resultLbl.isHidden = true
        hoursLbl.isHidden = true
        wageTxt.text = ""
        priceTxt.text = ""
    }
}

