//
//  Wage.swift
//  windowShopper
//
//  Created by Rei on 10/10/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation

class Wage {
    //funksioni merr dy parametra te tipit double dhe kthen nje int
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int{
        //konvertojme oret qe dalin nga funksioni ceil i cili rrumbullakos vleren
        return Int(ceil(price/wage))
    }
}
