//
//  CurrencyTextField.swift
//  windowShopper
//
//  Created by Rei on 10/10/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import UIKit
//per te pare ndryshimet ne storyboard krijojme @UIDesignable
 @IBDesignable
//kjo klase aplikohet mbi Text Fields
class CurrencyTextField: UITextField {

    
    //implementojme ne kohe reale dizajnin
    override  func prepareForInterfaceBuilder() {
        customizeView()
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
     customizeView()
        
    }
    
    func customizeView(){
        // background me transparence 25%
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.251005117)
            //vendosim cepat e lakuara
        layer.cornerRadius = 5.0
        //pozicionojme tekstin ne qender
        textAlignment = .center
        
        //nese placeholder nuk eshte nil
        if let p = placeholder {
            // percaktojme atributet
        let place = NSAttributedString(string: p, attributes: [.foregroundColor : UIColor.white ])
            //shtojme placeholderin
        attributedPlaceholder = place
            //ngjyra e tekstit do jete e bardhe
        textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }

        
        
    }
    
    
    

}
